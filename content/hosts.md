---
title: Hosts
tag: host
blurb: 
  Hosts are apps that let you use Audio Units to create new sounds, to apply effects to existing sounds. 
  They let you see and hear Audio Units, and organize them into projects or sessions.
  Some have built-in sequencing or note generation for instruments.
  Others let you apply effects to external audio signals.
---

Hosts are apps that let you use Audio Units to create new sounds, to apply effects to existing sounds. 
They let you see and hear Audio Units, and organize them into projects or sessions.
Some have built-in sequencing or note generation for instruments.
Others let you apply effects to external audio signals.
