---
title: Effects
tag: effect
blurb: 
  Once you have a sound from an Audio Unit instrument or elsewhere, you can pass it through an effect.
  Classic effects like flanger, delay, and echo are available.
  Other effects push the limits of digital processing and give you completely new sonic landscapes.
---

Once you have a sound from an Audio Unit instrument or elsewhere, you can pass it through an effect.
Classic effects like flanger, delay, and echo are available.
Other effects push the limits of digital processing and give you completely new sonic landscapes.
