---
title: What are Audio Units?
blurb: 
    Audio Units (or "Audio Unit Extensions", or "AUv3 Extensions") are used to connect music and audio apps on iOS (iPad and iPhone) devices.
    Use instruments to generate sounds, apply effects to those sounds, and organize them all using hosts.
    This website is dedicated to promoting the use of Audio Units with iOS.
---

Audio Unit Extensions, also known as Audio Units or AUv3 Extensions, are a way of connecting music and audio apps on iOS (iPad and iPhone) devices.
Use instruments to generate sounds, apply effects to those sounds, and organize them all using hosts.
You can even run more than one instance of a single app!

## What do I need to use Audio Units?

You'll need a device that supports iOS 9 or greater.
This includes the iPad 2, iPad mini, iPhone 4s, iPod Touch (5th generation), or any newer device.

Ideally, you'll want to use a device that supports iOS 11. [^1]
iOS 11 introduces new features for Audio Units.
It supports only 64-bit devices, which include the iPad Air, iPad mini 2, iPad Pro, iPhone 5s, iPod Touch (6th generation), or any newer device.

[^1]: iOS 11 has been a touchy subject in the iOS music world, as it drops support for 32-bit apps. While many developers have updated their apps to support 64-bit, some classics are sadly no longer available. If you have 32-bit apps that you absolutely *must* use, then keep at least one device running iOS 9 or 10.

## How do I get started using Audio Units?

Audio Units need a host in order for you to see and hear them. [^2]
Each Audio Unit extension has its own user interface, which is shown to you by the host.
Hosts connect Audio Units to the outside world of audio and MIDI.
Some have built-in sequencing or note generation tools for instruments. 
Others let you apply effects to an audio signal you provide elsewhere.
Here are some free hosts to get you started:

- [Free Audio Unit Hosts](/hosts/?free)

[^2]: Many apps that provide Audio Unit extensions can also be used in other modes, such as standalone, IAA (Inter App Audio), or AudioBus. To take full advantage of Audio Unit capabilities, such as multiple instances of a single app, you'll still need to use a host app.

Next, you'll need at least one instrument. 
Instruments generate sound, and are often driven by MIDI notes sent to it by the host.
They also have controls that can be modified by hand, or sequenced by the host.
Here are some free instruments you can play with:

- [Free Audio Unit Instruments](/instruments/?free)

Finally, you may want to add effects to your sound.
Classic effects like flanger, delay, and echo are available.
Other effects push the limits of digital processing and give you completely new sonic landscapes.
Here are some free effects to get you started:

- [Free Audio Unit Effects](/effects/?free)

## How can I develop an Audio Unit app?

As of right now, the Audio Unit Club website does not provide development services or advice.
We just love to support those who do by promoting their apps!

If you are interested in creating an app for iOS that provides an Audio Unit extension, these resources might help you get stated:

- [Apple Developer Documentation: AudioUnit Framework](https://developer.apple.com/documentation/audiounit)
- [Apple App Extension Programming Guide: Audio Unit](https://developer.apple.com/library/content/documentation/General/Conceptual/ExtensibilityPG/AudioUnit.html)

## What is Audio Unit Club?

Audio Unit Club is an independent website maintained by Matthew Scott.

We keep an ongoing list of apps that provide Audio Unit features.

For each app, we provide links to websites and videos that feature Audio Unit apps, such as reviews, musical performances, tutorials, and sound tests.
We then encourage visitors to purchase apps using affiliate links that support those third party authors.

Whenever we provide our own content, we will publish it ahead of third party content, and provide our own affiliate link. 
Third party affilate links will not be altered.

If you would like to write for our site, contact [matthew@audiounit.club](mailto:matthew@audiounit.club).
