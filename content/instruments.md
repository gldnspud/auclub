---
title: Instruments
tag: instrument
blurb:
  Instruments generate sound, and are often driven by MIDI notes sent to it by a host app.
  They have parameters that can be manipulated directly, or controlled by a host.
  Some instruments act much like classic synthesizers or samplers.
  Others find new uses of touch screen and digital synthesis to create unique sounds and experiences.
---

Instruments generate sound, and are often driven by MIDI notes sent to it by a host app.
They have parameters that can be manipulated directly, or controlled by a host.
Some instruments act much like classic synthesizers or samplers.
Others find new uses of touch screen and digital synthesis to create unique sounds and experiences.
