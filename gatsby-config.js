module.exports = {
  siteMetadata: {
    title: 'Audio Unit Club',
    siteUrl: 'http://www.audiounit.club',
    description: 'Information about Audio Unit compatible apps for iOS',
    defaultAffiliate: 'audiounitclub',
  },
  plugins: [
    'gatsby-transformer-auclub-affiliate',
    'gatsby-transformer-auclub-app',
    'gatsby-transformer-auclub-appstore-cache',
    'gatsby-transformer-auclub-music',
    {
      resolve: 'gatsby-plugin-typography',
      options: {
        pathToConfigModule: 'src/utils/typography.js',
      },
    },
    {
      resolve: 'gatsby-plugin-nprogress',
      options: {
        showSpinner: false,
      },
    },
    {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          'gatsby-remark-autolink-headers',
          'gatsby-remark-copy-linked-files',
          'gatsby-remark-emoji',
          'gatsby-remark-responsive-iframe',
          'gatsby-remark-smartypants',
          {
            resolve: 'gatsby-remark-images',
            options: {
              maxWidth: 590,
              linkImagesToOriginal: false,
            },
          },
        ],
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'affiliates',
        path: `${__dirname}/affiliates/`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'apps',
        path: `${__dirname}/apps/`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'appstore-cache',
        path: `${__dirname}/cache/appstore/`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'content',
        path: `${__dirname}/content/`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'music',
        path: `${__dirname}/music/`,
      },
    },
    'gatsby-plugin-antd',
    'gatsby-plugin-catch-links',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-twitter',
  ],
}

// https://www.gatsbyjs.org/packages/gatsby-plugin-feed/
// https://www.gatsbyjs.org/packages/gatsby-source-filesystem/
// https://www.gatsbyjs.org/packages/gatsby-plugin-nprogress/
// https://www.gatsbyjs.org/packages/gatsby-transformer-remark/

// TODO:
// https://www.gatsbyjs.org/packages/gatsby-plugin-sitemap/
// https://www.gatsbyjs.org/packages/gatsby-transformer-sharp/
// https://github.com/aquilio/gatsby-plugin-copy