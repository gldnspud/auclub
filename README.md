# Development Server

```
$ yarn develop
```

# Fetch info about all apps

```
$ yarn fetchapps
```

# Build for Deployment

```
$ yarn build
```
