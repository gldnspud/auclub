const { resolve } = require('path')
const { createFilePath } = require('gatsby-source-filesystem')

exports.onCreateNode = ({
  node,
  getNode,
  boundActionCreators,
}) => {
  const { createNodeField } = boundActionCreators
  if (node.internal.type === 'MarkdownRemark') {
    const path = createFilePath({ node, getNode, basePath: 'content' })
    createNodeField({
      node,
      name: 'path',
      value: path,
    })
  }
}

async function createPages({
  graphql,
  boundActionCreators,
}) {
  const { createPage } = boundActionCreators
  const {
    data: {
      allAuclubApp: {
        edges: appEdges,
      },
      allAuclubMusic: {
        edges: musicEdges,
      },
      allMarkdownRemark: {
        edges: topicEdges,
      },
    },
  } = await graphql(`
    {
      allAuclubApp {
        edges {
          node {
            slug
            trackId
          }
        }
      }
      allAuclubMusic {
        edges {
          node {
            slug
          }
        }
      }
      allMarkdownRemark {
        edges {
          node {
            id
            fields {
              path
            }
            frontmatter {
              tag
            }
          }
        }
      }
    }
  `)
  appEdges.forEach(
    ({ node: { slug, trackId } }) => createPage({
      path: `/apps/${slug}/`,
      component: resolve('./src/templates/app.js'),
      context: {
        trackId,
      },
    }),
  )
  musicEdges.forEach(
    ({ node: { slug } }) => createPage({
      path: `/music/${slug}/`,
      component: resolve('./src/templates/music.js'),
      context: {
        slug,
      },
    }),
  )
  topicEdges.forEach(
    ({ node: { id, fields: { path }, frontmatter: { tag } } }) => createPage({
      path,
      component: resolve('./src/templates/topic.js'),
      context: {
        contentId: id,
        tag,
      },
    }),
  )
}

exports.createPages = createPages
