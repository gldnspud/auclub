import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

const RenderedLink = styled.div`
  @media (min-width: 768px) {
    width: 45%;
  }
  ${({ fade }) => fade && `
    opacity: 0.2;
  `}
`

const Apps = styled.div`
  display: flex;
  @media (min-width: 768px) {
    flex-wrap: wrap;
  }
  @media (max-width: 767px) {
    flex-direction: column;
  }
`

export default ({
  apps,
  className,
  renderLink,
  sortBy,
  tags,
  freeOnly = typeof window !== 'undefined' && window.location.search === '?free',
}) => {
  tags.forEach(t => {
    apps = apps.filter(
      ({ tags }) => tags.indexOf(t) >= 0,
    )
  })
  apps.sort(({ [sortBy]: a, formattedPrice: p1 }, { [sortBy]: b, formattedPrice: p2 }) => {
    if (freeOnly && p1 === 'Free' && p2 !== 'Free') return 1
    if (freeOnly && p1 !== 'Free' && p2 === 'Free') return -1
    if (a < b) return -1
    if (a > b) return 1
    return 0
  })
  apps.reverse()
  return (
    <div {...{ className }}>
      {freeOnly && <p>(Free apps are highlighted)</p>}
      <Apps>
        {apps.map(app =>
          <RenderedLink
            fade={freeOnly && app.formattedPrice !== 'Free'}
            key={app.trackId}
          >
            {renderLink(app)}
          </RenderedLink>
        )}
      </Apps>
    </div>
  )
}
