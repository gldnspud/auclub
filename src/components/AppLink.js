import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import AppIcon from './AppIcon'

const LinkContainer = styled.div`
  display: flex;
  margin-bottom: .5rem;
`

const IconContainer = styled.div`
  line-height: 0;
`

const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  margin-left: .5rem;
  margin-right: .5rem;
`

const TrackName = styled.div`
  line-height: 100%;
  margin-bottom: .25rem;
`

const ArtistName = styled.div`
  line-height: 100%;
  font-size: smaller;
`

export default ({
  app,
  app: {
    artistName,
    slug,
    trackName,
  },
  size = 50,
}) =>
  <Link to={`/apps/${slug}/`}>
    <LinkContainer>
      <IconContainer>
        <AppIcon {...{ app, size }} />
      </IconContainer>
      <InfoContainer>
        <TrackName>{trackName}</TrackName>
        <ArtistName>by {artistName}</ArtistName>
      </InfoContainer>
    </LinkContainer>
  </Link>
