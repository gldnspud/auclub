import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  @media (max-width: 414px) {
    margin: .5rem;
  }
  @media (min-width: 415px) and (max-width: 768px) {
    margin: 1rem;
  }
  @media (min-width: 769px) {
    margin: 3rem;
  }
  @media (min-width: 1024px) {
    margin: 3rem auto;
    max-width: 920px;
  }
`

export default Container
