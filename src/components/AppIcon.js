import React from 'react'
import styled from 'styled-components'

const size = (div = 1) => ({ size }) => size / div

const RoundedImage = styled.img`
  margin: 0;
  padding: 0;
  width: ${size()}px;
  height: ${size()}px;
  min-width: ${size()}px;
  max-width: ${size()}px;
  min-height: ${size()}px;
  max-height: ${size()}px;
  border-radius: ${size(5)}px;
  @media (max-width: 320px) {
    width: ${size(2)}px;
    height: ${size(2)}px;
    min-width: ${size(2)}px;
    max-width: ${size(2)}px;
    min-height: ${size(2)}px;
    max-height: ${size(2)}px;
    border-radius: ${size(10)}px;
  }
`

export default ({
  app: {
    artworkUrl60,
    artworkUrl100,
    artworkUrl512,
    trackName,
  },
  size = 50,
}) =>
  <RoundedImage
    {...{ size }}
    src={
      size <= 30 ? artworkUrl60 : (
        size <= 50 ? artworkUrl100 : artworkUrl512
      )
    }
    alt={`Icon for ${trackName}`}
  />
