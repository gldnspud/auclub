import React, { Component } from 'react'
import styled from 'styled-components'
import ReactPlayer from 'react-player'

const IframeWrapper = styled.div`
  padding-bottom: ${({ height, width }) => height / width * 100}%;
  position: relative;
  height: 0;
  overflow: hidden;
`

class Player extends Component {
  render() {
    const {
      height,
      width,
      className,
      style,
      ...rest,
    } = this.props
    return (
      <IframeWrapper {...{ height, width, className, style }}>
        <ReactPlayer
          {...rest}
          controls
          height='100%'
          width='100%'
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
          }}
        />
      </IframeWrapper>
    )
  }
}

export default Player
