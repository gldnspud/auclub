export const affiliateMap = affiliateEdges => {
  const m = {}
  affiliateEdges.forEach(
    ({
      node,
      node: { affiliateId },
    }) =>
    {
      m[affiliateId] = node
    }
  )
  return m
}
