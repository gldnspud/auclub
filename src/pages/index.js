import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import AppLink from '../components/AppLink'
import AppLinks from '../components/AppLinks'

const appstoreMap = edges => {
  const m = {}
  edges.forEach(({ node, node: { trackId } }) => m[trackId] = node)
  return m
}

const Blurb = ({
  fields: {
    path,
  },
  frontmatter: {
    blurb,
    title,
  },
}) =>
  <div>
    <h1>{title}</h1>
    <p>{blurb}</p>
    <p><Link to={path}>
      <button>Learn more</button>
    </Link></p>
  </div>


const IndexPage = ({
  data: {
    allAuclubApp: {
      edges: clubEdges,
    },
    allAuclubAppstore: {
      edges: appstoreEdges,
    },
    allMarkdownRemark: {
      edges: markdownEdges,
    },
    markdownRemark: aboutNode,
  },
}) => {
  const map = appstoreMap(appstoreEdges)
  const app = node => ({
    ...node,
    ...map[node.trackId],
  })
  const apps = clubEdges.map(({ node }) => app(node))
  const section = tag =>
    <div>
      {
        markdownEdges
          .filter(({ node: { frontmatter: { tag: t } } }) => tag === t)
          .map(({ node, node: { fields: { path } } }) => <Blurb key={path} {...node} />)
      }
      <AppLinks
        {...{ apps }}
        tags={['au', tag]}
        sortBy='currentVersionReleaseDate'
        renderLink={app => <AppLink {...{ app }}/>}
      />
    </div>
  return (
    <div>
      <Blurb {...aboutNode} />
      {section('host')}
      {section('instrument')}
      {section('effect')}
    </div>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query IndexQuery {
    allAuclubApp {
      edges {
        node {
          slug
          trackId
          tags
        }
      }
    }
    allAuclubAppstore {
      edges {
        node {
          artistName
          artworkUrl60
          artworkUrl100
          artworkUrl512
          currentVersionReleaseDate
          formattedPrice
          trackId
          trackName
        }
      }
    }
    allMarkdownRemark(filter: { frontmatter: { tag: { ne: null } } }) {
      edges {
        node {
          fields {
            path
          }
          frontmatter {
            blurb
            tag
            title
          }
          html
        }
      }
    }
    markdownRemark(fields: { path: { eq: "/about/" } }) {
      fields {
        path
      }
      frontmatter {
        blurb
        title
      }
    }
  }
`
