import React from 'react'
import styled from 'styled-components'
import AppLink from '../components/AppLink'
import AppLinks from '../components/AppLinks'

const appstoreMap = edges => {
  const m = {}
  edges.forEach(({ node, node: { trackId } }) => m[trackId] = node)
  return m
}

export default ({
  data: {
    allAuclubApp: {
      edges: clubEdges,
    },
    allAuclubAppstore: {
      edges: appstoreEdges,
    },
    markdownRemark: {
      frontmatter: {
        tag,
        title,
      },
      html,
    },
  },
}) => {
  const map = appstoreMap(appstoreEdges)
  const app = node => ({
    ...node,
    ...map[node.trackId],
  })
  const apps = clubEdges.map(({ node }) => app(node))
  return (
    <div>
      <h1>{title}</h1>
      <div dangerouslySetInnerHTML={{ __html: html }}/>
      <AppLinks
        {...{ apps }}
        tags={['au', tag]}
        sortBy='currentVersionReleaseDate'
        renderLink={app => <AppLink {...{ app }}/>}
      />
    </div>
  )
}

export const pageQuery = graphql`
  query TopicQuery($contentId: String!) {
    allAuclubApp {
      edges {
        node {
          slug
          trackId
          tags
        }
      }
    }
    allAuclubAppstore {
      edges {
        node {
          artistName
          artworkUrl60
          artworkUrl100
          artworkUrl512
          currentVersionReleaseDate
          formattedPrice
          trackId
          trackName
        }
      }
    }
    markdownRemark(id: { eq: $contentId }) {
      frontmatter {
        tag
        title
      }
      html
    }
  }
`
