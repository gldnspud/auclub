import React from 'react'
import styled from 'styled-components'
import moment from 'moment'
import AppLink from '../components/AppLink'
import AppLinks from '../components/AppLinks'
import Player from '../components/Player'

const appSlugMap = edges => {
  const m = {}
  edges.forEach(({ node, node: { slug } }) => m[slug] = node)
  return m
}

const appstoreMap = edges => {
  const m = {}
  edges.forEach(({ node, node: { trackId } }) => m[trackId] = node)
  return m
}

const Title = styled.h1``

const By = styled.span`
  display: block;
  font-size: smaller;
  font-weight: normal;
  &:before {
    content: "by ";
  }
`

const Date = styled.div`
  font-size: smaller;
  line-height: 1;
  margin-bottom: 1rem;
`

export default ({
  data: {
    allAuclubApp: {
      edges: clubEdges,
    },
    allAuclubAppstore: {
      edges: appstoreEdges,
    },
    auclubMusic: {
      title,
      by,
      date,
      media,
      apps,
    },
  },
}) => {
  const storeMap = appstoreMap(appstoreEdges)
  const slugMap = appSlugMap(clubEdges)
  const app = slug => ({
    ...slugMap[slug],
    ...storeMap[slugMap[slug].trackId],
  })
  apps = apps.map(slug => app(slug))
  return (
    <div>
      <Title>
        {title}
        {by && <By>{by}</By>}
      </Title>
      <Date>{moment(date).format('ll')}</Date>
      <Player
        playsinline
        url={media}
        volume={1.0}
        width={16}
        height={9}
      />
      <h2>Featured Apps</h2>
      <AppLinks
        {...{ apps }}
        tags={['au']}
        sortBy='currentVersionReleaseDate'
        renderLink={app => <AppLink {...{ app }}/>}
      />
    </div>
  )
}

export const pageQuery = graphql`
  query MusicQuery($slug: String!) {
    allAuclubApp {
      edges {
        node {
          slug
          trackId
          tags
        }
      }
    }
    allAuclubAppstore {
      edges {
        node {
          artistName
          artworkUrl60
          artworkUrl100
          artworkUrl512
          currentVersionReleaseDate
          formattedPrice
          trackId
          trackName
        }
      }
    }
    auclubMusic(slug: { eq: $slug }) {
      title
      by
      date
      media
      apps
    }
  }
`
