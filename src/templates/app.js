import React, { Component } from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import AppIcon from '../components/AppIcon'
import BasePlayer from '../components/Player'
import moment from 'moment'
import { affiliateMap } from '../utils/affiliates'

const Banner = styled.div`
  display: flex;
  margin-bottom: 1rem;
`

const ArtistName = styled.span`
  font-size: smaller;
  font-weight: normal;
`

const InfoContainer = styled.h1`
  margin-top: 0;
  margin-bottom: 0;
  margin-left: .5rem;
  border: none; 
  align-self: center;
  @media (max-width: 320px) {
    font-size: 24px;
  }
`

const PlainTextLine = styled.span`
  display: block;
`

const CollapsedText = styled.div`
  position: relative;
  max-height: 10rem;
  overflow-y: hidden;
  &:after {
    content: "";
    position: absolute;
    z-index: 1;
    bottom: 0;
    left: 0;
    pointer-events: none;
    background-image: 
      linear-gradient(to bottom, rgba(255,255,255,0), rgba(255,255,255,1) 90%);
    width: 100%;
    height: 4rem;
  }
`

const Date = styled.div`
  font-size: smaller;
  line-height: 1;
  margin-bottom: 1rem;
`

const appStoreUrl =
  (trackId, at) =>
    `https://itunes.apple.com/us/app/id${trackId}?mt=8${at && `&at=${at}`}`

const CommissionNotice = styled.div`
  font-size: smaller;
  line-height: 2;
`

const PurchaseButton = styled(({
  className,
  style,
  affiliate,
  affiliates,
  formattedPrice,
  trackId,
}) => {
  const {
    at,
    name,
  } = affiliates[affiliate] || {}
  return (
    <div {...{ className, style }}>
      <div>
        <a href={appStoreUrl(trackId, at)}>
          <button>
            {
              formattedPrice === 'Free' ? (
                'Get it for Free'
              ) : (
                `Buy it for ${formattedPrice}`
              )
            }
          </button>
        </a>
      </div>
      {
        at && formattedPrice !== 'Free' && <CommissionNotice>
          ({name} will receive a small commission)
        </CommissionNotice>
      }
    </div>
  )
})`
  margin-top: 1rem;
  margin-bottom: 1rem;
`

class PlainText extends Component {
  state = {
    collapsed: true,
  }
  expand = () => this.setState({
    collapsed: false,
  })

  render() {
    const { children } = this.props
    if (children) {
      const { collapsed } = this.state
      const lines = children.split('\n')
      const plainTextLines = lines.map((line, i) =>
        <PlainTextLine key={i}>{line || <span>&nbsp;</span>}</PlainTextLine>,
      )
      return (lines.length > 3 && collapsed) ? (
        <div>
          <CollapsedText>{plainTextLines}</CollapsedText>
          <button onClick={this.expand}>Read more</button>
        </div>
      ) : (
        <div>{plainTextLines}</div>
      )
    } else {
      return <div>(None available)</div>
    }
  }
}

const Player = styled(BasePlayer)`
  margin-bottom: 1rem;
`

const Review = ({
  affiliates,
  defaultAffiliate,
  app,
  app: {
    trackName,
    formattedPrice,
  },
  review: {
    title,
    by,
    date,
    affiliate,
    post,
    media,
  },
}) => {
  const {
    name,
    at,
  } = affiliates[affiliate] || {}
  return (
    <div>
      <h3>{title}</h3>
      <Date>{moment(date).format('ll')}</Date>
      {
        post && <p>
          {
            affiliate ? (
              <span>
                Read <a target="_blank" href={post}>{by || name || affiliate}'s review of {trackName}</a>.
              </span>
            ) : (
              <span>
                Read <a target="_blank" href={post}>this review of {trackName}</a>.
              </span>
            )
          }
        </p>
      }
      {
        media && <div>
          {
            affiliate ? (
              <p>
                Watch {by || name || affiliate}'s video about {trackName}:
              </p>
            ) : (
              <p>
                Watch this video about {trackName}:
              </p>
            )
          }
          <Player
            playsinline
            url={media}
            volume={1.0}
            width={16}
            height={9}
          />
        </div>
      }
      {
        at && <p>
          <span>Did this review make you want to </span>
          {formattedPrice === 'Free' ? 'get' : 'buy'}
          <span> the app?</span>
        </p>
      }
      <PurchaseButton
        {...app} {...{ affiliates }}
        affiliate={at ? affiliate : defaultAffiliate}
      />
    </div>
  )
}

const Music = ({
  app,
  affiliates,
  defaultAffiliate,
  music: {
    slug,
    title,
    by,
    date,
    media,
    affiliate,
    apps,
  },
}) => {
  const {
    at,
  } = affiliates[affiliate] || {}
  return (
    <div>
      <h3>{title}</h3>
      <Date>{moment(date).format('ll')}</Date>
      <p>Watch this music video by {by}:</p>
      <Player
        playsinline
        url={media}
        volume={1.0}
        width={16}
        height={9}
      />
      {
        apps.length > 1 && <p>
          Learn about <Link to={`/music/${slug}/`}>other apps used in this video</Link>.
        </p>
      }
      {
        at && <p>
          <span>Did this music make you want to </span>
          {formattedPrice === 'Free' ? 'get' : 'buy'}
          <span> the app?</span>
        </p>
      }
      <PurchaseButton
        {...app} {...{ affiliates }}
        affiliate={at ? affiliate : defaultAffiliate}
      />
    </div>
  )
}

export default ({
  data: {
    site: {
      siteMetadata: {
        defaultAffiliate,
      },
    },
    allAuclubAffiliate: {
      edges: affiliateEdges,
    },
    allAuclubMusic: {
      edges: musicEdges,
    },
    auclubApp,
    auclubApp: {
      tags,
      reviews,
    },
    auclubAppstore,
    auclubAppstore: {
      artistName,
      description,
      releaseNotes,
      trackName,
    },
  },
}) => {
  const affiliates = affiliateMap(affiliateEdges)
  const app = { ...auclubAppstore, ...auclubApp }
  return (
    <div>
      <Banner>
        <AppIcon
          app={auclubAppstore}
          size={128}
        />
        <InfoContainer>
          {trackName}
          <br/>
          <ArtistName>by {artistName}</ArtistName>
        </InfoContainer>
      </Banner>
      <PurchaseButton {...app} {...{ affiliates }} affiliate={defaultAffiliate}/>
      {
        reviews && <p>
          Not sure yet?<br/><a href="#reviews">Check out some reviews &darr;</a>
        </p>
      }
      <h2>Description</h2>
      <PlainText>{description}</PlainText>
      <h2>Release Notes</h2>
      <PlainText>{releaseNotes}</PlainText>
      {
        reviews && <div id="reviews">
          <h2>Reviews</h2>
          {
            reviews.map(
              review =>
                <Review
                  {...{ affiliates, app, defaultAffiliate, review }}
                  key={`${review.title} ${review.affiliate}`}
                />,
            )
          }
        </div>
      }
      {
        musicEdges.length > 0 && <div id="music">
          <h2>Music</h2>
          {
            musicEdges.map(
              ({ node, node: { id } }) =>
                <Music
                  {...{ affiliates, app, defaultAffiliate }}
                  music={node}
                  key={id}
                />,
            )
          }
        </div>
      }
    </div>
  )
}

export const query = graphql`
  query AppQuery($trackId: Int!, $slug: [String]) {
    site {
      siteMetadata {
        defaultAffiliate
      }
    }
    allAuclubAffiliate {
      edges {
        node {
          affiliateId
          name
          at
          website
          youtube
          twitter
          facebook
          bandcamp
          soundcloud
          patreon
        }
      }
    }
    auclubApp(trackId: { eq: $trackId }) {
      slug
      tags
      reviews {
        title
        by
        date
        affiliate
        post
        media
      }
    }
    auclubAppstore(trackId: { eq: $trackId }) {
      artistName
      artworkUrl60
      artworkUrl100
      artworkUrl512
      description
      formattedPrice
      releaseNotes
      trackId
      trackName
    }
    allAuclubMusic(filter: { apps: { in: $slug } }) {
      edges {
        node {
          title
          by
          date
          media
          slug
          apps
        }
      }
    }
  }
`
