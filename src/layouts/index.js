import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import Container from '../components/Container'
import banner from '../banner.svg'
import '../polyfills'

const Banner = ({ title }) =>
  <Link to="/">
    <img src={banner} alt={title}/>
  </Link>

const Copyright = styled.footer`
  margin-top: 2rem;
  font-size: smaller;
  line-height: 1.2;
`

const TemplateWrapper = ({
  children,
  data: {
    site: {
      siteMetadata: {
        title,
      },
    },
  },
}) =>
  <Container>
    <Helmet
      {...{ title }}
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    />
    <Banner />
    <div>
      {children()}
    </div>
    <Copyright>
      <hr/>
      <div>
        Original content: Copyright &copy; 2017 by Matthew Scott.
      </div>
      <div>
        Third-party content used by permission or under fair use.
      </div>
    </Copyright>
  </Container>

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper

export const query = graphql`
  query LayoutQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
