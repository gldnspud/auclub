#!/usr/bin/env node --experimental-modules

// Fetches app info from iTunes API, saves it in `cache/appstore/`

import fs from 'fs'
import path from 'path'
import request from 'superagent'
import yaml from 'js-yaml'

const cacheDir = 'cache/appstore/'
const appsDir = 'apps/'

const pathnames = fs.readdirSync(appsDir).map(x => path.join(appsDir, x))

const ids =
  pathnames
    .map(p => yaml.safeLoad(fs.readFileSync(p, 'utf8')))
    .map(({ id }) => id)

const url = `https://itunes.apple.com/lookup?id=${ids.join(',')}`

const fetch = () =>
  request
    .get(url)
    .end((err, res) => {
      if (res.ok && !err) {
        const results = JSON.parse(res.text).results
        process(results)
      }
    })

const process = results =>
  results.forEach(result => {
    const resultText = yaml.safeDump(result)
    const { trackId } = result
    const cachePath = path.join(cacheDir, `${trackId}.yaml`)
    fs.writeFileSync(cachePath, resultText, 'utf8')
    console.log(`Wrote ${cachePath}`)
  })

fetch()
