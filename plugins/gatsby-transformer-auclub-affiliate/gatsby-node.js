const crypto = require('crypto')
const yaml = require('js-yaml')

async function onCreateNode({
  node,
  boundActionCreators,
  loadNodeContent,
}) {
  const { createNode } = boundActionCreators
  const { internal: { mediaType }, sourceInstanceName } = node
  if (mediaType !== 'text/yaml' || sourceInstanceName !== 'affiliates') {
    return
  }
  const content = await loadNodeContent(node)
  const parsed = yaml.safeLoad(content)
  const parsedStr = JSON.stringify(parsed)
  const contentDigest =
    crypto
      .createHash('md5')
      .update(parsedStr)
      .digest('hex')
  createNode({
    ...parsed,
    id: `${node.id} [${parsed.id}] >>> auclub-affiliate`,
    affiliateId: parsed.id,
    children: [],
    parent: node.id,
    internal: {
      contentDigest,
      type: 'AuclubAffiliate',
    },
  })
}

exports.onCreateNode = onCreateNode
