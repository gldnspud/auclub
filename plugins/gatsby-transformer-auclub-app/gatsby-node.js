const crypto = require('crypto')
const yaml = require('js-yaml')

const slug = p => require('path').basename(p, '.yaml')

async function onCreateNode({
  node,
  boundActionCreators,
  loadNodeContent,
}) {
  const { createNode } = boundActionCreators
  const {
    internal: {
      mediaType,
    },
    relativePath,
    sourceInstanceName,
  } = node
  if (mediaType !== 'text/yaml' || sourceInstanceName !== 'apps') {
    return
  }
  const content = await loadNodeContent(node)
  const parsed = yaml.safeLoad(content)
  const parsedStr = JSON.stringify(parsed)
  const contentDigest =
    crypto
      .createHash('md5')
      .update(parsedStr)
      .digest('hex')
  createNode({
    ...parsed,
    id: `${node.id} [${parsed.id}] >>> auclub-app`,
    trackId: parsed.id,
    children: [],
    parent: node.id,
    slug: slug(relativePath),
    internal: {
      contentDigest,
      type: 'AuclubApp',
    },
  })
}

exports.onCreateNode = onCreateNode
