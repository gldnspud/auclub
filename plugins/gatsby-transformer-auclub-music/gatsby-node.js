const crypto = require('crypto')
const yaml = require('js-yaml')

const slug = p => require('path').basename(p, '.yaml')

async function onCreateNode({
  node,
  boundActionCreators,
  loadNodeContent,
}) {
  const { createNode } = boundActionCreators
  const {
    internal: {
      mediaType,
    },
    relativePath,
    sourceInstanceName,
  } = node
  if (mediaType !== 'text/yaml' || sourceInstanceName !== 'music') {
    return
  }
  const content = await loadNodeContent(node)
  const parsed = yaml.safeLoad(content)
  const parsedStr = JSON.stringify(parsed)
  const contentDigest =
    crypto
      .createHash('md5')
      .update(parsedStr)
      .digest('hex')
  createNode({
    ...parsed,
    id: `${node.id} [${parsed.id}] >>> auclub-music`,
    children: [],
    parent: node.id,
    slug: slug(relativePath),
    internal: {
      contentDigest,
      type: 'AuclubMusic',
    },
  })
}

exports.onCreateNode = onCreateNode
